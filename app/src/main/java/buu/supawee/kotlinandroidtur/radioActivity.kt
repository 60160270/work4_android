package buu.supawee.kotlinandroidtur

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_radio.*

class radioActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio)
        radio_Group.setOnCheckedChangeListener{group,checkedId->
            val radio = findViewById<RadioButton>(checkedId)
            Toast.makeText(applicationContext,"On checked change : ${radio.text}",Toast.LENGTH_LONG).show()
        }
        btnColor.setOnClickListener{
            val id = radio_Group.checkedRadioButtonId
            if(id!=-1){
                val radio = findViewById<RadioButton>(id)
                Toast.makeText(applicationContext,"On button click ${radio.text}",Toast.LENGTH_LONG).show()
            } else{
                Toast.makeText(applicationContext,"On button click nothing selected",Toast.LENGTH_LONG).show()
            }
        }
    }

    fun radio_button_click (view: View){
        val radio = findViewById<RadioButton>(radio_Group.checkedRadioButtonId)
        Toast.makeText(applicationContext,"On Click : ${radio.text}",Toast.LENGTH_LONG).show()
    }
}