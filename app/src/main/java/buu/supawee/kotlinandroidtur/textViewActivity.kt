package buu.supawee.kotlinandroidtur

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class textViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_view)
        val textView = findViewById<TextView>(R.id.text_view)
        textView.setOnClickListener{
            Toast.makeText(MainActivity@this,R.string.text_on_click,Toast.LENGTH_LONG).show()
        }
    }
}