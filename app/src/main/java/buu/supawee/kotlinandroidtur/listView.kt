package buu.supawee.kotlinandroidtur

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class listView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        val arrayAdapter: ArrayAdapter<*>
        val users = arrayOf(
            "Supawee","Thanakrit","Yodsaporn","Jirapon","Sorachuch",
            "Supawee","Thanakrit","Yodsaporn","Jirapon","Sorachuch",
            "Supawee","Thanakrit","Yodsaporn","Jirapon","Sorachuch",
            "Supawee","Thanakrit","Yodsaporn","Jirapon","Sorachuch"
        )
        val userList = findViewById<ListView>(R.id.userList)
        arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,users)
        userList.adapter= arrayAdapter

        userList.setOnItemClickListener{parent ,view ,position,id ->
            Toast.makeText(this@listView," "+users[position],Toast.LENGTH_LONG).show()
            val intent = Intent(this@listView,showNameActivity::class.java)
            intent.putExtra("name" , users[position])
            startActivity(intent)
        }
    }
}