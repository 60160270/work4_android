package buu.supawee.kotlinandroidtur

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class showNameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_name)

        val name:String = intent.getStringExtra("name")?:"Unknown"
        val nameShow = findViewById<TextView>(R.id.showName)
        nameShow.text = name
    }
}