package buu.supawee.kotlinandroidtur

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_auto_complete_text_view.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnTextView = findViewById<Button>(R.id.btnTextView)
        btnTextView.setOnClickListener{
            val intent = Intent(MainActivity@this,textViewActivity::class.java)
            startActivity(intent)
        }

        val btnAutoActivity = findViewById<Button>(R.id.btnAutoActivity)
        btnAutoActivity.setOnClickListener{
            val intent = Intent(MainActivity@this,AutoCompleteTextView::class.java)
            startActivity(intent)
        }

        val btnCheckedText = findViewById<Button>(R.id.btnChecked)
        btnCheckedText.setOnClickListener{
            val intent = Intent(MainActiviy@this,checkedTextView::class.java)
            startActivity(intent)
        }

        val btnHorizontal = findViewById<Button>(R.id.btnHorizontal)
        btnHorizontal.setOnClickListener{
            val intent = Intent(MainActiviy@this,HorizontalScollView::class.java)
            startActivity(intent)
        }

        val btnList = findViewById<Button>(R.id.btnListView)
        btnList.setOnClickListener{
            val intent = Intent(MainActivity@this,listView::class.java)
            startActivity(intent)
        }

        val btnRadio = findViewById<Button>(R.id.btnRadio)
        btnRadio.setOnClickListener {
            val intent = Intent(MainActivity@this,radioActivity::class.java)
            startActivity(intent)
        }
    }
}